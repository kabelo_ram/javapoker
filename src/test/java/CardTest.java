import model.Card;
import model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardTest {



    @Test
    public void testCardToString() {


        Card card = new Card(1,1);
        String expected = "A of H";
        assertEquals(expected,card.toString());
    }



}