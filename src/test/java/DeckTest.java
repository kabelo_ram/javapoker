import model.Deck;
import model.Hand;
import common.Ranks;
import common.Suits;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DeckTest {

    @Test
    public void testHandFromArray() {

        int[] arr1 = new int[] {5,2,6,4,1,3};
    }

    @Test
    public void testDeck() {


        Deck deck = new Deck();
        int deckSize = Suits.suits.size() * Ranks.ranks.size();
        assertEquals(deckSize,deck.getTotalCards());
    }

    @Test
    public void testShuffleDeck() {


        Deck deck = new Deck();
        deck.shuffleCards();

        assertNotEquals(deck.getCards().get(0).getRank(),deck.getCards().get(1).getRank());
    }

    @Test
    public void testDealHand() {


        Deck deck = new Deck();
        Hand hand = deck.dealHand(5);

        assertEquals(5,hand.getCards().size());
    }




}