import service.PokerService;
import org.junit.Test;
import model.Hand;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class PokerServiceTest {

    @Test
    public void testSequential() {

        Integer[] arr1 = new Integer[] {5,2,6,4,1,3};
        Integer[] arr2 = new Integer[] {1,3,2};
        Integer[] arr3 = new Integer[] {1,2};
        Integer[] arr4 = new Integer[] {1};
        Integer[] arr5 = new Integer[] {1,7,2,9,12};

        PokerService.isSequential(arr1);
        assertEquals(true, PokerService.isSequential(arr1));
        assertEquals(true, PokerService.isSequential(arr2));
        assertEquals(true, PokerService.isSequential(arr3));
        assertEquals(true, PokerService.isSequential(arr4));
        assertEquals(false, PokerService.isSequential(arr5));
    }


    @Test
    public void testFlush() {
        //A flush is a hand that contains five cards all of the same suit, not all of sequential rank
        
        Hand hand = new Hand(new int[]{1,1,1,1,1},new int[]{1,7,3,4,5});
        Hand hand2 = new Hand(new int[]{1,1,1,2,1},new int[]{1,7,3,4,5});
        Hand hand3 = new Hand(new int[]{1,3,1,1},new int[]{1,7,3,4});
        Hand hand4 = new Hand(new int[]{2,2,2},new int[]{1,7,3});
        Hand hand5 = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,3,4,5});//this is straight flush

        assertEquals(true, PokerService.isFlush(hand));
        assertEquals(false, PokerService.isFlush(hand2));
        assertEquals(false, PokerService.isFlush(hand3));
        assertEquals(true, PokerService.isFlush(hand4));
       // assertEquals(false,KBPoker.isFlush(hand5));//this is straight flush
    }

    @Test
    public void testStraight() {
        // straight is a hand that contains five cards of sequential rank, not all of the same suit

        Hand hand = new Hand(new int[]{1,1,3,1,1},new int[]{7,8,9,11,10});//this is a straight
        Hand hand2 = new Hand(new int[]{1,1,1,2,1},new int[]{1,7,3,4,5});
        Hand hand3 = new Hand(new int[]{1,3,1,1},new int[]{1,4,3,2});//this is a straight
        Hand hand4 = new Hand(new int[]{2,2,2},new int[]{1,7,3});
        Hand hand5 = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,5,4,3});//this is straight flush

        assertEquals(true, PokerService.isStraight(hand));
        assertEquals(false, PokerService.isStraight(hand2));
        assertEquals(true, PokerService.isStraight(hand3));
        assertEquals(false, PokerService.isStraight(hand4));
    }


    @Test
    public void testStraightFlush() {
        //  straight flush is a hand that contains five cards of sequential rank, all of the same suit,

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,1,1,2,1},new int[]{1,7,3,4,5});
        Hand hand3 = new Hand(new int[]{1,3,1,1},new int[]{1,4,3,2});//this is a straight
        Hand hand4 = new Hand(new int[]{2,2,2},new int[]{1,7,3});
        Hand hand5 = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,5,4,3});//this is straight flush

        assertEquals(true, PokerService.isStraightFlush(hand));
        assertEquals(false, PokerService.isStraightFlush(hand2));
        assertEquals(false, PokerService.isStraightFlush(hand3));
        assertEquals(false, PokerService.isStraightFlush(hand4));
        assertEquals(true, PokerService.isStraightFlush(hand5));
    }



    @Test
    public void test4OfKind() {
        //  Four of a kind, also known as quads, is a hand that contains four cards of one rank and one card of another rank    

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,1,1,2,1},new int[]{7,7,7,7,5});
        Hand hand3 = new Hand(new int[]{1,3,1,3,1,3,2},new int[]{4,4,7,6,5,4,4});//this is 4 of kind
        Hand hand4 = new Hand(new int[]{1,1,1,2,1},new int[]{1,7,3,4,5});
        Hand hand5 = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,5,4,3});//this is straight flush

        assertEquals(false, PokerService.is4OfKind(hand));
        assertEquals(true, PokerService.is4OfKind(hand2));
        assertEquals(true, PokerService.is4OfKind(hand3));
        assertEquals(false, PokerService.is4OfKind(hand4));
        assertEquals(false, PokerService.is4OfKind(hand5));
    }

    @Test
    public void testFullHouse() {
        //  A full house is a hand that contains three cards of one rank and two cards of another rank

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,7,5});
        Hand hand3 = new Hand(new int[]{1,3,1,3,1,3,2},new int[]{4,4,7,6,5,4,4});//this is 4 of kind
        Hand hand4 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,3,6,5});
        Hand hand5 = new Hand(new int[]{1,1,1,1,2,3,1},new int[]{1,2,5,2,3,2,3});//this is straight flush

        assertEquals(false, PokerService.isFullHouse(hand));
        assertEquals(true, PokerService.isFullHouse(hand2));
        assertEquals(false, PokerService.isFullHouse(hand3));
        assertEquals(false, PokerService.isFullHouse(hand4));
        assertEquals(true, PokerService.isFullHouse(hand5));
    }


    @Test
    public void testIs3OfKind() {
        //  A Three of a kind is a hand that contains three cards of one rank and two cards of two other ranks (the kickers)

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,7,5});
        Hand hand3 = new Hand(new int[]{1,3,1,3,1,3,2},new int[]{4,4,7,6,5,9,4});//this is 3 of kind
        Hand hand4 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,6,6,5});
        Hand hand5 = new Hand(new int[]{1,1,1,1,2,3,1},new int[]{1,2,5,2,3,2,3});//this is straight flush

        assertEquals(false, PokerService.is3OfKind(hand));
        assertEquals(false, PokerService.is3OfKind(hand2));
        assertEquals(true, PokerService.is3OfKind(hand3));
        assertEquals(true, PokerService.is3OfKind(hand4));
    }

    @Test
    public void testIs2Pair() {
        //  A Two pair is a hand that contains two cards of one rank, two cards of another rank and one card of a third rank (the kicker)

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,5});
        Hand hand3 = new Hand(new int[]{1,3,1,3,1,3,2},new int[]{4,9,7,6,5,9,4});
        Hand hand4 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,6,6,5});
        Hand hand5 = new Hand(new int[]{1,1,1,1,2,3,1},new int[]{1,2,5,2,3,2,3});//this is straight flush

        assertEquals(false, PokerService.is2Pair(hand));
        assertEquals(true, PokerService.is2Pair(hand2));
        assertEquals(true, PokerService.is2Pair(hand3));
        assertEquals(false, PokerService.is2Pair(hand4));
    }

    @Test
    public void testIs1Pair() {
        //  One pair, or simply a pair, is a hand that contains two cards of one rank and three cards of three other ranks (the kickers)

        Hand hand = new Hand(new int[]{3,3},new int[]{7,8});//this is a straight flush
        Hand hand2 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,9});
        Hand hand3 = new Hand(new int[]{1,3,1,3,1,3,2},new int[]{4,9,7,6,5,9,4});
        Hand hand4 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,6,6,5});
        Hand hand5 = new Hand(new int[]{1,1,1,1,2,3,1},new int[]{1,2,5,2,3,2,3});//this is straight flush

        assertEquals(false, PokerService.is1Pair(hand));
        assertEquals(true, PokerService.is1Pair(hand2));
        assertEquals(false, PokerService.is1Pair(hand3));
        assertEquals(false, PokerService.is1Pair(hand4));
        assertEquals(true, PokerService.is1Pair(hand5));
    }

    @Test
    public void testEvaluateHand() {
        //  One pair, or simply a pair, is a hand that contains two cards of one rank and three cards of three other ranks (the kickers)

        Hand hand = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,5,4,3});//this is straight flush
        Hand hand2 = new Hand(new int[]{1,1,1,1,1},new int[]{1,7,3,4,5});//flush
        Hand hand1 = new Hand(new int[]{1,1,3,1,1},new int[]{7,8,9,11,10});//this is a straight
        Hand hand3 = new Hand(new int[]{1,1,1,2,1},new int[]{7,7,7,7,5});// 4 of kind
        Hand hand4 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,7,5});// full house
        Hand hand5 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,6,6,5}); // 3 of kind
        Hand hand6 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,5}); // 2 pair
        Hand hand7 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,9}); // 1 pair
        Hand hand8 = new Hand(new int[]{1,2,3,4,1},new int[]{7,5,3,12,9}); //high card

        assertEquals("Straight Flush", PokerService.evaluateHand(hand));
        assertEquals("Four of a Kind", PokerService.evaluateHand(hand3));
        assertEquals("Full House", PokerService.evaluateHand(hand4));
        assertEquals("Flush", PokerService.evaluateHand(hand2));
        assertEquals("Straight", PokerService.evaluateHand(hand1));
        assertEquals("Three of a Kind", PokerService.evaluateHand(hand5));
        assertEquals("Two Pair", PokerService.evaluateHand(hand6));
        assertEquals("One Pair", PokerService.evaluateHand(hand7));
        assertEquals("High Cards", PokerService.evaluateHand(hand8));

    }

    @Test
    public void testCountDuplicates() {
        //  One pair, or simply a pair, is a hand that contains two cards of one rank and three cards of three other ranks (the kickers)

        Hand hand = new Hand(new int[]{1,1,1,1,1},new int[]{1,2,5,4,3});//this is straight flush
        Hand hand3 = new Hand(new int[]{1,1,1,2,1},new int[]{7,7,7,7,5});// 4 of kind
        Hand hand4 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,7,5});// full house
        Hand hand5 = new Hand(new int[]{1,1,1,2,1},new int[]{6,7,6,6,5}); // 3 of kind
        Hand hand6 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,5}); // 2 pair
        Hand hand7 = new Hand(new int[]{1,3,1,1,2},new int[]{7,5,7,12,9}); // 1 pair
        Hand hand8 = new Hand(new int[]{1,2,3,4,1},new int[]{7,5,3,12,9}); //high card

        assertEquals(true, PokerService.countDuplicates(hand6,2,2));
        assertEquals(true, PokerService.countDuplicates(hand7,2,1));
        assertEquals(true, PokerService.countDuplicates(hand5,3,1));

    }

}