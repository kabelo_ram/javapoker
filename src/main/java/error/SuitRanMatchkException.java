package error;

public class SuitRanMatchkException extends RuntimeException {
    public SuitRanMatchkException(String errorMessage) {
        super(errorMessage);
    }
}
