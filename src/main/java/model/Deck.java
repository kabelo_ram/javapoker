package model;

import common.Ranks;
import common.Suits;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
@Getter
public class Deck {
    private List<Card> cards;


    /**
     * Create a new Deck of Cards
     */
    public Deck()
    {
        //using linked list makes program more flexible to future changes like allowing players to swap cards in their hand
        cards = new LinkedList<Card>();

        for (int a = 1; a <= Suits.suits.size(); a++)
        {
            for (short b = 1; b <= Ranks.ranks.size(); b++)
            {
                cards.add( new Card(a,b) );
            }
        }
    }

    /**
     * Shuffles cards in deck and places them in random order
     */
    public void shuffleCards(){

        System.out.println("Shuffling...Shuffling...Shuffling...");
        int index_1, index_2;
        Random generator = new Random();
        Card temp;

        int size = cards.size() -1;

        for (int i=0; i<100; i++)
        {
            index_1 = generator.nextInt( size );
            index_2 = generator.nextInt( size );

            temp = (Card) cards.get( index_2 );
            cards.set( index_2 , cards.get( index_1 ) );
            cards.set( index_1, temp );
        }
    }

    /**
     * Removes card from the back of deck
     * @return a random card from the deck
     */
    public Card drawFromDeck()
    {
        return cards.remove( cards.size()-1 );
    }

    public int getTotalCards()
    {
        return cards.size();
    }

    /**
     * deal a hand of n cards from the deck
     * @param numCardsInHand the number of cards to deal
     * @return the dealt hand
     */
    public Hand dealHand(int numCardsInHand){
        Hand hand = new Hand();

        for(int i = 0; i < numCardsInHand; i++)
            hand.addCard(this.drawFromDeck());

        return hand;
    }
}