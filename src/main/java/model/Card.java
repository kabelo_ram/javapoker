package model;

import common.Ranks;
import common.Suits;
import lombok.Data;

@Data
public class Card {
    private int rank, suit;

    public Card(int suit, int rank)
    {
        this.rank=rank;
        this.suit=suit;
    }

    public @Override String toString()
    {
        return Ranks.ranks.get(rank) + " of " + Suits.suits.get(suit);
    }


}