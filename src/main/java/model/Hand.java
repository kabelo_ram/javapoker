package model;

import error.SuitRanMatchkException;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
public class Hand {
    private List<Card> cards;

    public Hand(){
        cards = new LinkedList<Card>();
    }


    /**
     * Add new card to hand
     * @param suit the suit of the card
     * @param rank the rank of th card
     */
    public void addCard(int suit, int rank){
        cards.add(new Card(suit,rank));
    }


    /**
     * Add new card to hand
     * @param card to be added to hand
     */
    public void addCard(Card card){
        cards.add(card);
    }

    /**
     * Creates new hand from array of suits and ranks
     * @param suits the suits of the cards
     * @param ranks the ranks of the cards
     */
    public  Hand(int[] suits, int[] ranks){
        cards = new LinkedList<Card>();
        if(suits.length == ranks.length){
            for(int i = 0; i < suits.length; i++){
                cards.add(new Card(suits[i],ranks[i]));
            }
        }else
        {
            throw new SuitRanMatchkException("number of suits must match number of ranks");
        }
    }

    /**
     * Gets the suits of all cards in the hand
     * @return A list of suits for cards in the hand
     */
    public List<Integer> getSuits(){
        List<Integer> suits = new LinkedList<Integer>();

        for(int i = 0;i < cards.size(); i++)
            suits.add(cards.get(i).getSuit());

        return suits;
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    /**
     * Gets the ranks of all cards in the hand
     * @return a list of all cards in the hand
     */
    public List<Integer> getRanks(){
        List<Integer> ranks = new LinkedList<Integer>();

        for(int i = 0;i < cards.size(); i++)
            ranks.add(cards.get(i).getRank());

        return ranks;
    }
}
