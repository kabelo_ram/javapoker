package setup;

import model.Deck;
import model.Hand;
import service.PokerService;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {


        boolean keepGoing = true;

        System.out.println("** Hello and welcome to Advanced Poker **\n");
        while(keepGoing){
            System.out.println("\n#@#@#@# MAIN MENU #@#@#@#");
            System.out.println("Enter the number of the option below");
            System.out.println("(1) Play Poker ");
            System.out.println("(2) Exit program");

            try {
                Scanner scanner = new Scanner(System.in);
                int menu1Selection = scanner.nextInt();
                scanner.skip("(\r\n|[\n\r])?");//nextInt leaves line terminators behind so skip them

                switch (menu1Selection){
                    case 1:
                        System.out.println("\nStarting poker game");
                        playPoker(scanner);
                        break;
                    case 2:
                        System.out.println("\nExit");
                        keepGoing = false;
                        scanner.close();
                        break;
                    default:
                        System.out.println("You have not selected a valid option, please try again\n");
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have made an illegal selection, please try again\n");
            }catch (Exception e){
                System.out.println("There was an error processing an entry, please try again or exit program\n");
            }
        }

    }

    public static void playPoker(Scanner scanner){
        boolean continueCmdLine = true;


        while(continueCmdLine){

            System.out.println("Enter in the number of cards per hand (minimum 4)");

            int numCardsPerhand = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r])?");//nextInt leaves line terminators behind so skip them

            if(numCardsPerhand >= 4){
                Deck deck = new Deck();
                deck.shuffleCards();
                Hand hand = deck.dealHand(numCardsPerhand);
                String result = PokerService.evaluateHand(hand);
                System.out.println("Your hand: " + hand);
                System.out.println("You have: " + result);
                System.out.println("\nDo you want to deal a new hand?");
                System.out.println("Enter any character for yes");
                System.out.println("Enter (N) for no");

                String selection = scanner.nextLine();

                if(selection.equalsIgnoreCase("N")){
                    continueCmdLine = false;
                }else{

                }
            }else{
                System.out.println("The number you entered is too low");
            }

        }
    }

}
