package service;

import model.Card;
import model.Hand;

import java.util.*;
import java.util.Map.Entry;

/**
 * This class has all the poker evaluation logic
 * For console program, the use of static methods is fine as an object of PokerService is not needed to evaluate a hand
 *
 * When upgrading the service to run on a back end for a web based service, Poker service shall changed into a bean
 * and injected into the classes that need its functionality. At this point all static methods can be changed to non static
 */
public class PokerService {

    /**
     * Evaluates a given poker hand and returns its poker rank
     * @param hand the hand to be evaluated
     * @return the poker rank
     */
    public static String evaluateHand(Hand hand)
    {
        String handResult = "";

        if(isStraightFlush(hand)){
            handResult = "Straight Flush";
        }else if(is4OfKind(hand)){
            handResult = "Four of a Kind";
        }else if(isFullHouse(hand)){
            handResult = "Full House";
        }else if(isFlush(hand)){
            handResult = "Flush";
        }else if(isStraight(hand)){
            handResult = "Straight";
        }else if(is3OfKind(hand)){
            handResult = "Three of a Kind";
        }else if(is2Pair(hand)){
            handResult = "Two Pair";
        }else if(is1Pair(hand)){
            handResult = "One Pair";
        }else{
            handResult = "High Cards";
        }

        return handResult;
    }

    /**
     * Checks if an array is in sequence, i.e if each element is 1 more than the previous element
     * @param arr
     * @return
     */
    public static Boolean isSequential(Integer[] arr) {
        Arrays.sort(arr);
        boolean sequential = true;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i +1] - 1)
                sequential = false;
        }
        return sequential;
    }

/*    public static boolean isStraightFlush(){

    }*/

    /**
     * A flush is a hand that contains five cards all of the same suit, not all of sequential rank
     * @param hand a hand of cards
     * @return true if flush else false
     */
    public static boolean isFlush(Hand hand){

        List<Integer> suits = hand.getSuits();
        Collections.sort(suits);
        //if first suit the same as last after sorting then all cards have the same suite
        return(suits.get(0) == suits.get(suits.size() - 1) );
    }
    
    /**
     * A straight is a hand that contains five cards of sequential rank, not all of the same suit
     * @param hand a hand of cards
     * @return true if straight else false
     */
    public static boolean isStraight(Hand hand){

        List<Integer> ranks = hand.getRanks();
        Integer[] rankArray = new Integer[ranks.size()];
        ranks.toArray(rankArray);
        return isSequential(rankArray); //&& !isFlush(hand);
    }

    /**
     * A straight flush is a hand that contains five cards of sequential rank, all of the same suit,
     * @param hand a hand of cards
     * @return true if straight flush else false
     */
    public static boolean isStraightFlush(Hand hand){

        return isStraight(hand) && isFlush(hand);
    }

    /**
     * Four of a kind, also known as quads, is a hand that contains four cards of one rank and one card of another rank
     * @param hand a hand of cards
     * @return true if 4 of kind else false
     */
    public static boolean is4OfKind(Hand hand){

        boolean is4OfKind = false;

        if(hand.getCards().size() >= 4){
            List<Integer> ranks = hand.getRanks();
            Map<Integer, Integer> rankDuplicateMap = new HashMap<Integer,Integer>();

            for(Integer rank: ranks){
                if(rankDuplicateMap.containsKey(rank)){
                    int prevDuplicateCount = rankDuplicateMap.get(rank);
                    rankDuplicateMap.put(rank, (prevDuplicateCount + 1));
                }else{
                    rankDuplicateMap.put(rank, 1);
                }
            }

            List<Entry<Integer, Integer>> listOfDuplicates = new ArrayList<>(rankDuplicateMap.entrySet());
            listOfDuplicates.sort(Entry.comparingByValue());

            Entry<Integer, Integer> mostReapeated = listOfDuplicates.get(listOfDuplicates.size()-1);

            if(mostReapeated.getValue() == 4){
                is4OfKind = true;
            }
        }
        return is4OfKind;
    }

    /**
     * A full house is a hand that contains three cards of one rank and two cards of another rank,
     * @param hand a hand of cards
     * @return true if full house else false
     */
    public static boolean isFullHouse(Hand hand){

        boolean isFullHouse = false;

        if(hand.getCards().size() >= 4){
            List<Integer> ranks = hand.getRanks();
            Map<Integer, Integer> rankDuplicateMap = new HashMap<Integer,Integer>();

            for(Integer rank: ranks){
                if(rankDuplicateMap.containsKey(rank)){
                    int prevDuplicateCount = rankDuplicateMap.get(rank);
                    rankDuplicateMap.put(rank, (prevDuplicateCount + 1));
                }else{
                    rankDuplicateMap.put(rank, 1);
                }
            }

            Map<Integer, Integer> swapped = new HashMap<>();

            for(Entry<Integer, Integer> entry: rankDuplicateMap.entrySet()){
                swapped.put(entry.getValue(),entry.getKey());
            }


            if(swapped.containsKey(3) && swapped.containsKey(2)){
                isFullHouse = true;
            }
        }
        return isFullHouse;
    }

    /**
     * Three of a kind is a hand that contains three cards of one rank and two cards of two other ranks (the kickers)
     * @param hand a hand of cards
     * @return true if 3 of kind else false
     */
    public static boolean is3OfKind(Hand hand){

        boolean is3OfKind = false;

        if(hand.getCards().size() >= 4){

            if ( is4OfKind(hand) || isFullHouse(hand) ){
                is3OfKind = false;//the card is better
            }else{
                is3OfKind =  countDuplicates(hand,3,1);//duplicate count = 2 because we are looking for pairs
            }

        }
        return is3OfKind;
    }

    /**
     *   A Two pair is a hand that contains two cards of one rank, two cards of another rank and one card of a third rank (the kicker)
     * @param hand a hand of cards
     * @return true if 2 pair else false
     */
    public static boolean is2Pair(Hand hand){

        boolean is2Pair = false;

        if(hand.getCards().size() >= 4){
            is2Pair = countDuplicates(hand,2,2);//duplicate count = 2 because we are looking for pairs
        }
        return is2Pair;
    }

    /**
     *   One pair, or simply a pair, is a hand that contains two cards of one rank and three cards of three other ranks (the kickers)
     * @param hand a hand of cards
     * @return true if 1 pair else false
     */
    public static boolean is1Pair(Hand hand){

        boolean is1Pair = false;

        if(hand.getCards().size() >= 4){
            is1Pair = countDuplicates(hand,2,1);//duplicate count = 2 because we are looking for pairs
        }
        return is1Pair;
    }

    public static boolean countDuplicates(Hand hand,int duplicateCount, int numberOfPairs){

        boolean isPair = false;
        List<Integer> ranks = hand.getRanks();
        Map<Integer, Integer> rankDuplicateMap = new HashMap<Integer,Integer>();
        //count duplicates
        for(Integer rank: ranks){
            if(rankDuplicateMap.containsKey(rank)){
                int prevDuplicateCount = rankDuplicateMap.get(rank);
                rankDuplicateMap.put(rank, (prevDuplicateCount + 1));
            }else{
                rankDuplicateMap.put(rank, 1);
            }
        }


        Map<Integer, Integer> swapped = new HashMap<>();
        //count duplicate groups
        for(Entry<Integer, Integer> entry: rankDuplicateMap.entrySet()){

            if(swapped.containsKey(entry.getValue())){
                int prevDuplicateCount = swapped.get(entry.getValue());
                swapped.put(entry.getValue(),(prevDuplicateCount + 1));
            }else{
                swapped.put(entry.getValue(), 1);
            }

        }

        if(swapped.containsKey(duplicateCount) && (swapped.get(duplicateCount) == numberOfPairs)){
            isPair = true;//there are #duplicateCount duplicate groups that repeat #duplicateCount times
        }

        return isPair;
    }

}
