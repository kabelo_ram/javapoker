package common;

import java.util.HashMap;
import java.util.Map;

public class Ranks {

    public static Map<Integer,String> ranks = new HashMap<Integer,String>();

    static{
//these would be better placed in a DB to allow change to ranks without redeploy
        ranks.put(1,"A");
        ranks.put(2,"2");
        ranks.put(3,"3");
        ranks.put(4,"4");
        ranks.put(5,"5");
        ranks.put(6,"6");
        ranks.put(7,"7");
        ranks.put(8,"8");
        ranks.put(9,"9");
        ranks.put(10,"10");
        ranks.put(11,"J");
        ranks.put(12,"Q");
        ranks.put(13,"K");
    }
}
