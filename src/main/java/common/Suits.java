package common;

import java.util.HashMap;
import java.util.Map;

public class Suits {

    public static Map<Integer,String> suits = new HashMap<Integer,String>();
    //these would be better placed in a DB to allow change to suits without redeploy
    static{
        suits.put(1,"H");
        suits.put(2,"S");
        suits.put(3,"D");
        suits.put(4,"C");
    }
}
